from django.shortcuts import render
from django.http import JsonResponse
import requests
import json


def home(request):
    
    return render(request, 'main/home.html')
   
    

def data(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    data = json.loads(ret.content)
    return JsonResponse(data, safe=False)
