from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import resolve
from http import HTTPStatus
from .views import home, data



class MainTestCase(TestCase):

    def test_url_root(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    
    def test_url_root_using_home_function_in_views(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_url_root_using_home_template (self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'main/home.html')
    
    def test_url_data(self):
        response = Client().get('/data?q=/')
        self.assertEqual(response.status_code, 301)
    
    def test_url_data_using_data_function_in_views(self):
        found = resolve('/data/')
        self.assertEqual(found.func, data)
    
    

    




        


